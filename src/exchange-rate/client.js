const axios = require('axios');

class ExchangeRate {
    constructor() {
        this.client = axios.create({
            baseURL: 'https://api.exchangerate.host'
        });
    }

    latest() {
       return this.client
        .get('/latest', {
            params: {
                base: 'USD',
            }
        })
        .then(({ data }) => data)
    }
}

module.exports = ExchangeRate;