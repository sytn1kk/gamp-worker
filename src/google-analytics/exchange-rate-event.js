class ExchangeRateEvent {
    constructor(exchangeRate) {
        this.name = 'exchange_rates';
        this.params = {
            currency: exchangeRate.base,
            date: exchangeRate.date,
            value: exchangeRate.rates['UAH'],
        }
    }
}

module.exports = ExchangeRateEvent;