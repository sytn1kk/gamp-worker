const axios = require('axios');
const { v4: uuidv4 } = require('uuid');

class GoogleAnalytics {
    constructor() {
        this.client = axios.create({
            baseURL: 'https://www.google-analytics.com',
            params: {
                measurement_id: process.env.GA_ID,
                api_secret: process.env.GA_TOKEN,
            }
        });
    }

    send(event) {
        console.log(process.env.GA_ID)
        console.log(process.env.GA_TOKEN)
        return this.client.post('/mp/collect', {
            client_id: uuidv4(),
            events: [event],
        })
    }
}


module.exports = GoogleAnalytics;