require('dotenv').config({ path: __dirname + '/../.env' });

const ExchangeRate = require('./exchange-rate/client');
const GoogleAnalytics = require('./google-analytics/client');

const ExchangeRateEvent = require('./google-analytics/exchange-rate-event');

async function main() {
    const exchangeRate = new ExchangeRate();
    const googleAnalytics = new GoogleAnalytics();

    const latest = await exchangeRate.latest();
    await googleAnalytics.send(new ExchangeRateEvent(latest))

    console.log(latest);
}

main();