FROM node:lts

WORKDIR /app

COPY package.json /app/

RUN npm install

COPY . .

RUN apt-get update && apt-get -y install cron

COPY ./cron/exchange-rate /etc/cron.d/exchange-rate

RUN chmod 0644 /etc/cron.d/exchange-rate

RUN crontab /etc/cron.d/exchange-rate

RUN touch /var/log/cron.log

CMD cron && tail -f /var/log/cron.log