# Google Analytics Measurement Protocol
## Google Analytics 4, Measurement Protocol, Node.js


## Run

- ```git clone https://gitlab.com/sytn1kk/gamp-worker.git```
- ```cd gamp-worker```
- ```docker build -t gamp-worker . && docker run -it gamp-worker```

## Screenshots
![Exchange rates](https://i2.paste.pics/dd04615ae98847b4ebd2375525cfa191.png "Exchange rates")
![Realtime events](https://i2.paste.pics/4a2567ac99aaa97d2082aab9179be9b9.png "Realtime events")

